﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace OfficeSyncHelper
{
    class Program
    {
        // version
        static readonly string version = "\nOfficeSyncHelper - 1.0\n";

        // default paths
        static string iacaExportFile = "C:\\Users\\Cpasjuste\\Desktop\\iaca_export.csv";
        static string officeExportFile = "C:\\Users\\Cpasjuste\\Desktop\\office_export.csv";
        static string officeHeader = "Nom d’utilisateur,Prénom,Nom de famille,Afficher le nom,Poste,Département,Numéro du bureau,Téléphone(bureau),Téléphone mobile, Fax, Adresse email de secours,Adresse,Ville,Etat ou province,Code postal, Pays ou région";

        // use iso-8859-1 encoding
        static Encoding encoding = Encoding.GetEncoding("iso-8859-1");

        public class Csv
        {
            public string path;
            public string header;
            public string buffer;
            public List<string> lines = new List<string>();

            public Csv(string p, string h)
            {
                path = p;
                header = h;
            }

            public Csv(string p, Encoding e)
            {
                path = p;
                StreamReader sr = new StreamReader(p, e);
                header = sr.ReadLine();
                buffer = sr.ReadToEnd();
                lines = buffer.Split('\n', StringSplitOptions.RemoveEmptyEntries).ToList();
                sr.Close();
            }

            public bool Save()
            {
                try
                {
                    StreamWriter sw = new StreamWriter(path, false, encoding);
                    sw.WriteLine(header);
                    foreach (string line in lines)
                    {
                        sw.WriteLine(line);
                    }
                    sw.Close();
                }
                catch (Exception)
                {
                    return false;
                }

                return true;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine(version);

#if !DEBUG
            // sanity checks
            if (args == null || args.Length != 2)
            {
                Console.WriteLine("\nusage: OfficeSyncHelper.exe iaca_export.csv office_export.csv\n");
                return;
            }

            iacaExportFile = args[0];
            officeExportFile = args[1];
#endif

            if (!File.Exists(iacaExportFile))
            {
                Console.WriteLine("\nerror: iaca export file not found: " + iacaExportFile);
                return;
            }

            if (!File.Exists(officeExportFile))
            {
                Console.WriteLine("\nerror: office export file not found: " + iacaExportFile);
                return;
            }

            // read the whole iaca export in mem
            Console.Write("- Parsing iaca export... ");
            Csv iacaCsv = new Csv(iacaExportFile, encoding);
            Console.Write(iacaCsv.lines.Count + " users found.\n");

            // read the whole office export in mem
            Console.Write("- Parsing office export... ");
            Csv officeCsv = new Csv(officeExportFile, encoding);
            Console.Write(officeCsv.lines.Count + " users found.\n");

            // create office 365 import file(s)
            Console.Write("- Creating office import... ");
            string ext = Path.GetExtension(iacaCsv.path);
            int splitCount = 0;
            int userCount = 0;
            Csv csv = new Csv(iacaCsv.path.Replace(ext, "_" + splitCount + ext), officeHeader);
            for (int i = 0; i < iacaCsv.lines.Count; i++)
            {
                // office import limited to 249 accounts...
                if (userCount > 0 && userCount % 249 == 0)
                {
                    splitCount++;
                    csv.Save();
                    csv = new Csv(iacaCsv.path.Replace(ext, "_" + splitCount + ext), officeHeader);
                }

                // add user to office import file if mail not found
                string mail = iacaCsv.lines[i].Split(',')[0];
                if (!officeCsv.buffer.Contains(mail))
                {
                    csv.lines.Add(iacaCsv.lines[i].Trim() + ",,,,,,,,,,,");
                    userCount++;
                }
            }
            csv.Save();

            // all done
            Console.Write(userCount + " users written.\n\nAll done...\n");
        }
    }
}
